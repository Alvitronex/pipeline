FROM node:16.13.2-alpine3.15 as build
WORKDIR /app
COPY . .
RUN yarn install
RUN yarn build

FROM node:16.13.2-alpine3.15
RUN apk --no-cache --update add build-base bash \
    libzip-dev busybox-suid shadow dumb-init
ENV PATH /app/node_modules/.bin:$PATH
RUN adduser -D -u 1001 nuxt && usermod -aG 0 nuxt
WORKDIR /app
RUN chown -R 1001 /app && chgrp -R 0 /app && chmod -R g=u /app
USER 1001
COPY package.json yarn.lock ./
RUN yarn install --production
COPY --from=build /app/.nuxt /app/.nuxt
COPY . .
CMD ["dumb-init","nuxt","start"]
EXPOSE 3000